package gr.demokritos.workreports.data.entities;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

public class EntityToPersistListener {

    @PrePersist
    @PreUpdate
    public void methodExecuteBeforeSave(final AbstractEntity entity) {
        //Make any change to the entity such as calculation before the save process
        if (entity.getId() == null) {
            entity.setCreationDate(LocalDateTime.now());
//            entity.setCreatedBy(username);
        }
        entity.setModificationDate(LocalDateTime.now());
//        entity.setModifiedBy(username);
    }
}
