package gr.demokritos.workreports.data.services;

import gr.demokritos.workreports.data.entities.User;
import gr.demokritos.workreports.data.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Service
public class UserService extends CrudService<User, Integer> {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private final UserRepository repository;

    public UserService(@Autowired UserRepository repository) {
        this.repository = repository;
    }

    public List<User> getAllUsers() {
        return this.repository.findAll();
    }

    public User getUserByEmail(String email) {
        return this.repository.findByEmail(email);
    }

    public User getUserByUsername(String username) {
        return this.repository.findByUsername(username);
    }

    public User register(String username, String password, String email, String firstName, String lastName) {
        if (emailExists(email) || usernameExists(username)) {
            User user = new User();
            if (emailExists(email)) {
                user.setResponse("Email exists");
            } else {
                user.setResponse("Username exists");
            }
        }
        User user = new User(firstName, lastName, username, password, email);
        user.setPassword(hashPassword(password));
        return repository.save(user);
    }

    public User login(String username, String password) {
        User user = repository.findByUsername(username);
        if (user == null) {
            user = new User();
            user.setResponse("Username does not exist");
            return user;
        }
        if (!checkPassword(password, user.getPassword())) {
            user = new User();
            user.setResponse("Password is not valid");
            return user;
        }
        if (!user.getActive()) {
            user = new User();
            user.setResponse("User is not active");
            return user;
        }
        user.setLastLogin(LocalDateTime.now());
        return repository.save(user);
    }

    @Override
    public User update(User user) {
        try {
            User compareUser = repository.findByUsername(user.getUsername());
            if (compareUser != null && !compareUser.getId().equals(user.getId())) {
                logger.error("Username already exists");
                return null;
            }
            user.setPassword(hashPassword(user.getPassword()));
            user = repository.save(user);
            logger.debug("Successfully updated user " + user.getUsername());
            return user;
        } catch (Exception e) {
            logger.error("Something went wrong while updating user " + user.getUsername());
            logger.error(e.getMessage());
            try {
                throw new Exception(e);
            } catch (Exception exception) {
                exception.printStackTrace();
                return null;
            }
        }
    }

    public Boolean deleteUser(String username) {
        try {
            User user = repository.findByUsername(username);
            repository.delete(user);
            return true;
        } catch (Exception e) {
            logger.error("Something went wrong while deleting user with username: " + username);
            logger.error(Arrays.toString(e.getStackTrace()));
            return false;
        }
    }

    private Boolean emailExists(String email) {
        User user = repository.findByEmail(email);
        return user != null;
    }

    private Boolean usernameExists(String username) {
        User user = repository.findByUsername(username);
        return user != null;
    }

    private boolean checkPassword(String plainTextPassword, String hashedPassword) {
        return BCrypt.checkpw(plainTextPassword, hashedPassword);
    }

    private String hashPassword(String passwordForHashing) {
        return BCrypt.hashpw(passwordForHashing, BCrypt.gensalt(10));
    }

    @Override
    protected UserRepository getRepository() {
        return repository;
    }

}
